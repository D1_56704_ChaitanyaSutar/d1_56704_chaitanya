
CREATE TABLE movie(movie_id INT PRIMARY KEY AUTO_INCREMENT, movie_title VARCHAR(200), movie_release_date VARCHAR(100), movie_time VARCHAR(100), director_name VARCHAR(100));