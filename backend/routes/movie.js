const express = require('express')
const db = require("./../db")
const router = express.Router()
const utils = require("../utils");

router.get("/",(request,response)=>{
   const {movie_title} = request.body 
   const statement = `SELECT movie_title, movie_release_date, movie_time, director_name FROM movie WHERE movie_title = '${movie_title}'`

   db.execute(statement, (error,result)=>{
        if(error){
            response.send(utils.createResult(error))
        }
        else{
           if(result.length > 0){
               response.send(utils.createResult(error,result[0]))
           }else{
               response.send(utils.createResult("name not found"))
           }
        }
   })

})
// . GET  --> Display Movie using name from Containerized MySQL

// 2. POST --> ADD Movie data into Containerized MySQL table

// 3. UPDATE --> Update Release_Date and Movie_Time into Containerized MySQL table

// 4. DELETE --> Delete Movie from Containerized MySQL

router.post("/",(request,response)=>{
    const {movie_title, movie_release_date, movie_time, director_name} = request.body
    const statement = `INSERT INTO movie(movie_title, movie_release_date, movie_time, director_name) VALUES('${movie_title}', '${movie_release_date}','${movie_time}', '${director_name}')`
    
    db.execute(statement, (error, result)=>{
        if(error){
            response.send(utils.createResult(error))
        }
        else{
               response.send(utils.createResult(error,result))
           }
        
    })
})

router.put("/:movie_id",(request,response)=>{
    const {movie_release_date, movie_time} = request.body
    const {movie_id} = request.params
    const statement = `UPDATE movie SET movie_release_date = '${movie_release_date}', movie_time = '${movie_time}' WHERE movie_id = ${movie_id}`

    db.execute(statement, (error, result)=>{
        if(error){
            response.send(utils.createResult(error))
        }
        else{
               response.send(utils.createResult(error,result))
           }
    })
})

router.delete("/:movie_id",(request,response)=>{
    const {movie_id} = request.params
    const statement = `DELETE FROM movie WHERE movie_id = ${movie_id}`

    db.execute(statement, (error, result)=>{
        if(error){
            response.send(utils.createResult(error))
        }
        else{
               response.send(utils.createResult(error,result))
           }
    })
})

module.exports = router